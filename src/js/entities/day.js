/**
 * @author Denis Golikov <golikov25rus@gmail.com>
 */

import {days} from '../enums/week.js';

/**
 * @namespace App~Weather
 * @class Day
 * @desc
 * Сущность для дней. Содержит в себе дату и прогноз погоды
 */
export default class Day {
    /**
     * @access public
     * @param {Date} date - объект даты
     * @param {{object}} weather - key=>value объект с информацией о погоде
     */
    constructor(date, weather) {
        /**
         * @access private
         * @type {Date}
         */
        this.dateObject = date;
        /**
         * @access private
         * @type {{object}}
         */
        this.weather = weather;
    }

    /**
     * @access public
     * @readonly
     * @return {string} - дата в формате дд.мм.гггг
     */
    get date() {
        return this.dateObject.toLocaleDateString();
    }

    /**
     * @access public
     * @readonly
     * @borrows App~Weather~Enums~Days
     * @return {string} - день недели в формате "Понедельник, вторник..." и т.д.
     */
    get name() {
        return days[this.dateObject.getDay()];
    }
}