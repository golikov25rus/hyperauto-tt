/**
 * @author Denis Golikov <golikov25rus@gmail.com>
 */

/**
 * @namespace App~Weather
 * @class City
 * @desc
 * Сущность для городов. Содержит в себе адрес и координаты
 */
export default class City {
    /**
     * @access public
     * @param {Array} address - массив сегментов адреса
     * @param {{lat: number, lon: number}} coordinates - широта и долгота
     */
    constructor(address, coordinates) {
        /**
         * @access private
         * @type {Array}
         */
        this.address = address;
        /**
         * @access public
         * @type {{lat: number, lon: number}}
         */
        this.coordinates = coordinates;
    }

    /**
     * @access public
     * @readonly
     * @return {string} - адрес
     * @desc
     * Получение строкового эквивалента названия адреса с соединением сегментов
     * через запятую
     */
    get name() {
        return this.address.join(', ');
    }
}