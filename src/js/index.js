import Processing from './controllers/processing.js';
import Render from './controllers/render.js';
import CityTemplate from './views/city.js';
import WeatherSliderTemplate from './views/weatherSlider.js';
import DayTemplate from './views/day.js';

((w) => {
    w.onload = function () {
       let mapVendor = window.ymaps;
       new Processing({
            controls: {
                button: '#searchButton',
                input: '#cityInput',
                selector: '#temperatureType'
            },
            vendors: {
                map: mapVendor,
                weather: {
                    endpoint: '/api?',
                    language: 'ru_RU',
                    key: 'e8513bef-f1de-4576-bb8a-d433ba1122a0'
                }
            },
            render: new Render({
                elements: {
                    messageBox: '#message',
                    weatherWrapper: '#weatherWrapper',
                    spinner: '#spinner'
                },
                tempAttribute : 'temperature',
                templates: {
                    city: CityTemplate,
                    slider: WeatherSliderTemplate,
                    day: DayTemplate
                }
            }),
           defaults: {
               temperatureType: 'celsius',
               startMessage: 'Для поиска могут быть использованы любые географические названия и имена'
           }
        })
    }
})(window);

