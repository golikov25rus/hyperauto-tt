/**
 * @author Denis Golikov <golikov25rus@gmail.com>
 */

/**
 * @namespace App
 * @class Render
 * @desc
 * Класс взаимодействия с GUI
 */
export default class Render {
    /**
     * @access public
     * @param {{messageBox: string, weatherWrapper: string, spinner: string}} elements - строковые селекторы GUI
     * @param {string} tempAttribute - атрибут, в котором хранится альтернативная температура (F/C)
     * @param {{city: CityTemplate, slider: WeatherSliderTemplate, day: DayTemplate}} templates - шаблоны верстки
     * @desc
     * Класс взаимодействует с указанными элементами, изменяя их атрибуты или удаляя или создавая их потомков с помощью
     * предустановленных шаблонов
     * @todo Добавить универсальный метод рендеринга произвольного шаблона в произвольном месте с произвольными данными аля TWIG
     */
    constructor({elements, tempAttribute, templates}) {
        /**
         * @access private
         * @type {Element | null}
         */
        this.weatherWrapper = document.querySelector(elements.weatherWrapper);
        /**
         * @access private
         * @type {Element | null}
         */
        this.spinner = document.querySelector(elements.spinner);
        /**
         * @access private
         * @type {Element | null}
         */
        this.messageBox = document.querySelector(elements.messageBox);
        /**
         * @access private
         * @type {string}
         */
        this.tempAttribute = tempAttribute;
        /**
         * @access private
         * @type {{city: CityTemplate, slider: WeatherSliderTemplate, day: DayTemplate}}
         */
        this.templates = templates;
        /**
         * @access public
         * @type {number}
         */
        this.citiesCount = 0;
    }

    /**
     * @access public
     * @param {Element[]} elements
     * @param {?boolean} [state = null]
     * @return void
     * @desc
     * Переключатель атрибута disable. По умолчанию работает в режиме toggle. При явном указании
     * state - (true->disable | false->active)
     */
    toggleDisabling([...elements], state = null) {
        for(let element of elements) {
            switch (state) {
                case null:
                    if(element.hasAttribute('disabled')) {
                        element.removeAttribute('disabled');
                    } else {
                        element.setAttribute('disabled', '');
                    }
                    break;
                case true:
                    element.setAttribute('disabled', '');
                    break;
                case false:
                    element.removeAttribute('disabled');
                    break;
            }
        }
    }

    /**
     * @access public
     * @return void
     * @desc
     * Переключатель температуры. Поиск графических елементов производится селектором, так как
     * их количество изменяется в зависимости от пользовательского запроса
     */
    toggleTemperature() {
        let elements = document.querySelectorAll(`[${this.tempAttribute}]`);
        for(let element of elements) {
            let elementTemp = element.attributes[this.tempAttribute].textContent;
            element.attributes[this.tempAttribute].textContent = element.textContent;
            element.textContent = elementTemp;
        }
    }

    /**
     * @access public
     * @param {string} message
     * @return void
     * @desc
     * Отображает сообщение в предустановленном блоке
     */
    set message(message) {
        this.messageBox.textContent = message;
    }

    /**
     * @access public
     * @borrows App~Render~message
     * @param {boolean} value
     * @return void
     * @desc
     * Отображает сообщение в предустановленном блоке
     */
    set showSpinner(value) {
        this.message = '';
        if(value) {
            this.spinner.classList.remove('hidden')
        } else {
            this.spinner.classList.add('hidden')
        }
    }

    /**
     * @access public
     * @borrows App~Weather
     * @param {Weather} weather
     * @return void
     * @desc
     * Отрисовывает модель погоды в с использованием шаблона
     */
    set weather(weather) {
        this.showSpinner = false;
        let daysHTML = '';
        for (let day of weather.daysWeather) {
            daysHTML += this.templates.day(day);
        }
        let sliderHTML = this.templates.slider(daysHTML),
            cityElement = document.createElement('div');
        cityElement.innerHTML = this.templates.city(weather, sliderHTML);
        this.weatherWrapper.appendChild(cityElement);
        this.citiesCount++;
    }

    /**
     * @access public
     * @borrows App~Weather
     * @return void
     * @desc
     * Удаляем дочерние элементы
     */
    clean() {
        while(this.citiesCount > 0) {
            this.weatherWrapper.removeChild(this.weatherWrapper.children[0]);
            this.citiesCount--;
        }
    }
}