/**
 * @author Denis Golikov <golikov25rus@gmail.com>
 */

import MapProvider from "../services/mapProvider";
import WeatherProvider from "../services/weatherProvider";
import City from "../entities/city";
import Weather from "../models/weather";

/**
 * @namespace App
 * @class Processing
 * @desc
 * Центральный класс приложения, реализующий логику приложения
 */
export default class Processing {
    /**
     * @access public
     * @param {{button: string, input: string, selector:string}} controls - селекторы GUI контролов
     * @param {{map: object, weather: object}} vendors
     * @param {Render} render
     * @param {{temperatureType: string, startMessage: string}} - тип температуры и сообщение по умолчанию
     * @desc
     * В качестве конфигурации для инициализации принимает строковые селекторы, объекты поставщиков данных и настройки к ним,
     * и объект для работы с отрисовкой а так же параметры по умолчанию
     */
    constructor({controls, vendors, render, defaults}) {
        /**
         * @access private
         * @type {{HTMLElement}}
         */
        this.controlElements = this.initElements(controls);
        /**
         * @access private
         * @type {{map: MapProvider, weather: WeatherProvider}}
         */
        this.providers = this.initProviders(vendors);
        /**
         * @access private
         *  @type {Render}
         */
        this.render = render;
        /**
         * @access private
         *  @type {string}
         */
        this.temperatureType = defaults.temperatureType;

        this.inputHandler(this.controlElements.input, this.controlElements.button);
        this.selectorHandler(this.controlElements.selector);
        this.render.message = defaults.startMessage;
    }

    /**
     * @access private
     * @param {{string}} selectors
     * @return {{HTMLElement}}
     * @desc
     * Получение DOM объектов по строковым селекторам, установка слушателей
     */
    initElements(selectors) {
        let result = {};
        for(let [key, value] of Object.entries(selectors)) {
                result[key] = document.querySelector(value);
        }
        result.button.addEventListener('click', event => this.clickHandler(event, this.controlElements.input));
        result.input.addEventListener('keyup', event => this.clickHandler(event, this.controlElements.input));
        result.input.addEventListener('input', event => this.inputHandler(event.target, this.controlElements.button));
        result.selector.addEventListener('change', event => this.selectorHandler(event.target));
        return result;
    }

    /**
     * @access private
     * @param {{object}} vendors
     * @return {{map: MapProvider, weather: WeatherProvider}}
     * @desc
     * Инициализация провайдерских объектов
     */
    initProviders(vendors) {
        return {
            map: new MapProvider(vendors.map),
            weather: new WeatherProvider(vendors.weather)
        }
    }

    /**
     * @access private
     * @param {string} string
     * @return {string}
     * @desc
     * Обрезает пробельные и цифровые символы в начале и конце строки оставляя только один пробел в конце строки
     */
    inputTrim(string) {
        return string.replace(/^[\d\s]+/g, '')
            .replace(/[\d]+$/g, '')
            .replace(/\s{2}$/g, ' ');
    }

    /**
     * @access private
     * @borrows App~Render
     * @param {(Event~target|HTMLSelectElement)} element
     * @return void
     * @desc
     * Обработчик изменений в селекторе температуры. Получает значение выбранного типа температуры.
     * Если на странице уже отрисована погода, то изменяет тип температуры с блокировкой
     * всех GUI контроллеров на время выполнянения изменений
     */
    selectorHandler(element) {
        this.temperatureType = element.children[element.selectedIndex].value;
        if(this.render.citiesCount > 0) {
            this.render.toggleDisabling(Object.values(this.controlElements));
            this.render.toggleTemperature();
            this.render.toggleDisabling(Object.values(this.controlElements));
        }
    }

    /**
     * @access private
     * @param {(Event~target|HTMLInputElement)} input
     * @param {HTMLButtonElement} button
     * @return void
     * @desc
     * Обрабатывает пользовательский ввод и переключает состояние GUI контролов.
     */
    inputHandler(input, button) {
        input.value = this.inputTrim(input.value);
        if(input.value.length > 0) {
            this.render.toggleDisabling([button], false);
        } else {
            this.render.toggleDisabling([button], true);
        }
    }

    /**
     * @access private
     * @borrows App~Render
     * @borrows App~MapProvider
     * @param {Event} event
     * @param {HTMLInputElement} input
     * @return void
     * @desc
     * Обрабатывает пользовательский клик и нажатие кнопки Enter.
     * Переключает состояние GUI контролов и отправляет данные и коллбэк для получения вендором геоданных
     * и последующей их обработку в {@link Processing~responseHandler}.
     */
    clickHandler(event, input) {
        if(event.target === input && event.keyCode !== 13) {
            return;
        }
        this.render.toggleDisabling(Object.values(this.controlElements));
        this.render.showSpinner = true;
        this.providers.map.getCities(input.value, this.responseHandler.bind(this));
        this.render.clean();
    }

    /**
     * @access public
     * @async
     * @callback
     * @borrows App~Render
     * @borrows App~providers~weather
     * @param {(string|City|Weather)} result
     * @param {boolean} state
     * @return void
     * @desc
     * Callback для обработки результатов работы вендеров. При этом вендор вторым аргументом
     * возвращает boolean, который сообщает о статусе обработки.
     * Если операция прошла успешно(true), то в зависимости от типа объекта в result, данный метод
     * либо посылает новые запросы, либо отдает {@link App~render} объекты на отрисовку.
     * Если операция прошла не успешно, то в result будет string сообщение, которое так
     * же будет отдано на отрисовку {@link App~render}
     */
    responseHandler(result, state) {
        if(state) {
            switch (result.__proto__.constructor.name) {
                case 'City':
                    this.providers.weather.getWeather(result, this.responseHandler.bind(this), this.temperatureType);
                    break;
                case 'Weather':
                    this.render.toggleDisabling(Object.values(this.controlElements), false);
                    this.render.weather = result;
                    break;
            }
        } else {
            this.render.toggleDisabling(Object.values(this.controlElements), false);
            this.render.showSpinner = false;
            this.render.message = result;
        }
    }
}