DayTemplate = (day) => {
    let forecast = day.weather,
        primaryTemp = forecast.temperature[forecast.temperature.primary],
        alternativeTemp = forecast.temperature[forecast.temperature.alternative],
        waterTemperatureMarkup = /^\d/.test(primaryTemp.water) ? `<div><span>Температура воды: </span><span temperature="${alternativeTemp.water}">${primaryTemp.water}</span></div>`:'';

    return `
        <li>
            <div class="uk-card uk-card-default uk-animation-slide-top-medium uk-card-hover">
                <div class="uk-card-header uk-padding-small">
                    <div class="uk-grid-small uk-flex-middle" uk-grid>
                        <div class="uk-width-auto">
                            <img class="uk-border-circle" width="40" height="40" src="${forecast.iconPath}">
                        </div>
                        <div class="uk-width-expand text-left">
                            <div class="uk-background-primary text-white text-center light-corner" date="${day.name}">${day.date}</div>
                            <div>
                                ${forecast.condition}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-card-body uk-padding-small">
                    <div>
                        <span>Температура: </span><span temperature="${alternativeTemp.air}">${primaryTemp.air}</span>
                    </div>
                    <div>
                        <span>Ветер: </span><span>${forecast.wind.speed}</span>
                    </div>
                    <div>
                        <span>Направление: </span><span>${forecast.wind.direction}</span>
                    </div>
                    <div>
                        <span>Влажность: </span><span>${forecast.humidity}</span>
                    </div>
                    <div>
                        <span>Давление: </span><span>${forecast.pressure}</span>
                    </div>                            
                    ${waterTemperatureMarkup}
                </div>
            </div>
        </li>        
    `;
};
module.exports = DayTemplate;