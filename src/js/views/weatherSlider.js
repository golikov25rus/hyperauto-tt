WeatherSliderTemplate = (elements) => {

    return `        
    <div uk-slider="autoplay: true center: true" class="left-padding">
        <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
            <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@xl uk-grid uk-grid-small">
                ${elements}
            </ul>
            <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>
        <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
    </div>      
    `;
};
module.exports = WeatherSliderTemplate;