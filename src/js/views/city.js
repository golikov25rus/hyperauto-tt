CityTemplate = (weather, slider) => {
    let forecast = weather.currentDay.weather,
        primaryTemp = forecast.temperature[forecast.temperature.primary],
        alternativeTemp = forecast.temperature[forecast.temperature.alternative],
        waterTemperatureMarkup = /^\d/.test(primaryTemp.water) ? `<div><span>Температура воды: </span><span temperature="${alternativeTemp.water}">${primaryTemp.water}</span></div>`:'';

    return `
<div class="uk-card uk-card-default uk-padding-remove uk-animation-slide-right-medium uk-card-hover city">
            <div class="uk-card-header">
                <div class="uk-grid-small uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid>
                    <div class="uk-width-1-5@s">
                        <img class="uk-border-circle" width="100" height="100" src="${forecast.iconPath}">
                        <div>
                            ${forecast.condition}
                        </div>
                    </div>
                    <div class="uk-width-expand">
                        <h3 class="uk-card-title uk-margin-remove-bottom">${weather.city.name}</h3>
                    </div>
                    <div class="uk-width-auto">
                        <div class="uk-background-primary text-white text-center light-corner uk-padding-small text-bold date" date="${weather.currentDay.name}">${weather.currentDay.date}</div>
                    </div>
                </div>
                <div class="uk-grid-small uk-child-width-1-2@s" uk-grid>
                    <div>
                        <div>
                            <span>Температура: </span><span temperature="${alternativeTemp.air}">${primaryTemp.air}</span>
                        </div>
                        <div>
                            <span>Ветер: </span><span>${forecast.wind.speed}</span>
                        </div>
                        <div>
                            <span>Направление: </span><span>${forecast.wind.direction}</span>
                        </div>
                    </div>
                    <div>
                        <div>
                            <span>Влажность: </span><span>${forecast.humidity}</span>
                        </div>
                        <div>
                            <span>Давление: </span><span>${forecast.pressure}</span>
                        </div>
                        ${waterTemperatureMarkup}
                    </div>
                </div>
            </div>
            <div class="uk-card-body uk-padding-remove uk-background-muted">
                <p class="uk-padding-small uk-text-large">Погода в ближайшие дни</p>
                ${slider}
            </div>
        </div>
`;
};
module.exports = CityTemplate;