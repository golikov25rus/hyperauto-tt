/**
 * @author Denis Golikov <golikov25rus@gmail.com>
 */

/**
 * @namespace App~Weather~Enums
 */

/**
 * @readonly
 * @name Enums~Days
 * @enum {string}
 */
export const days = {
    1: 'Понедельник',
    2: 'Вторник',
    3: 'Среда',
    4: 'Четверг',
    5: 'Пятница',
    6: 'Суббота',
    0: 'Воскресенье'
};