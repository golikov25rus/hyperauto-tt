/**
 * @author Denis Golikov <golikov25rus@gmail.com>
 */
import Weather from '../models/weather.js';

/**
 * @namespace App
 * @class WeatherProvider
 * @desc
 * Провайдер для {@link Weather} по средствам AJAX запросов на сервис Яндекс.Погода
 * Удаленный вендор не поддерживает клиентские запросы, поэтому в рамках ТЗ запросы идут на webpack devServer
 * где проксируются на Яндекс.
 * @see API документации {@link https://tech.yandex.ru/weather/doc/dg/concepts/about-docpage/}
 * @todo Перенести хранение ключа на сервер
 */
export default class WeatherProvider {
    /**
     * @access public
     * @param {{endpoint: string, language: string, key: string}} vendor - конфиги настройки для общения с удаленным вендором
     * @desc
     * Эндпоинт должен принимать GET запросы. В качестве аргумента для инициализации передаются все
     * сегменты от корня до знака вопроса, например "/path/from/root"
     */
    constructor(vendor) {
        this.endpoint = vendor.endpoint;
        this.key = vendor.key;
        this.language = vendor.language;
    }

    /**
     * @access public
     * @async
     * @borrows App~Weather
     * @param {City} city - город по которому нужно получить погоду
     * @param {function} callBack - обработчик
     * @param {string} primaryTempType - тип температуры "celsius\fahrenheit"
     * @return void
     * @desc
     * Отправка AJAX запросов на сервер вендора. Создает объекты погоды в случае успешного запроса или отправляет
     * сообщение о неудачном запросе
     */
    getWeather(city, callBack, primaryTempType) {
        let lat = city.coordinates.lat,
            lon = city.coordinates.lon,
            xhr = new XMLHttpRequest(),
            request = `${this.endpoint}lat=${lat}&lon=${lon}&lang=${this.language}`;

        xhr.open("GET", request, true);
        xhr.setRequestHeader("X-Yandex-API-Key", this.key);

        xhr.onload = function() {
            callBack(new Weather(JSON.parse(this.response), city, primaryTempType), true);
        };

        xhr.onerror = function() {
            let result = `Не удалось получить прогноз погоды по запросу: ${cityName}.`;
            callBack(result, false);
        };
        xhr.send();
    }
}