/**
 * @author Denis Golikov <golikov25rus@gmail.com>
 */
import City from '../entities/city.js'

/**
 * @namespace App
 * @class MapProvider
 * @desc
 * Провайдер для {@link City}, является оберткой над глобальным классом вендора Яндекс.Карты
 * @see API документации {@link https://tech.yandex.ru/maps/doc/jsapi/2.1/quick-start/index-docpage/}
 */
export default class MapProvider {
    /**
     * @access public
     * @param {{ymaps}} mapVendor - объект вендора Яндекс.Карты
     * @desc
     * Объект Яндекс.Карт достается из глобальной области видимости
     */
    constructor(mapVendor){
        /**
         * @access private
         * @type {{ymaps}}
         */
        this.vendor = mapVendor;
        /**
         * @access private
         * @type {function}
         */
        this.callBack = ()=>{};
    }

    /**
     * @access public
     * @async
     * @param {string} cityName - строка пользовательского ввода
     * @param {function} callBack - обработчик
     * @return void
     * @desc
     * Отправка запроса на Яндекс.Карты с вызовом в последующем либо обработку ошибки либо обработку
     * ответа
     */
    getCities(cityName, callBack) {
        this.callBack = callBack;
        this.vendor.geocode(cityName, {json: true})
            .then((response) => {
                        this.responseHandler(response.GeoObjectCollection.featureMember, cityName);
                    },
                    (error) => {
                        this.errorHandler(error, cityName)
                    });
    }

    /**
     * @access private
     * @async
     * @callback
     * @param {JSON} geoCollection - коллекция гео объектов полученых от вендора
     * @param {string} cityName
     * @return void
     * @desc
     * Обработчик ответов от вендора, который на запрос возвращает коллекцию геообъектов.
     * В случае, если коллеция не пуста, создаем города в методе createCity и передаем обработчику.
     * В противном случае - отправляем сообщение, что по запросу ничего не найдено
     */
    responseHandler(geoCollection, cityName) {
        let result = {};
        if(geoCollection.length === 0) {
            result = `Не удалось получить данные по запросу: ${cityName}`;
            this.callBack(result, false);
        }
        for(let object of geoCollection) {
            let result = this.createCity(object.GeoObject.metaDataProperty.GeocoderMetaData);
            this.callBack(result, true);
        }
    }

    /**
     * @access private
     * @async
     * @param {string} message - сообщение об ошибке
     * @param {string} cityName
     * @return void
     * @desc
     * Обработчик ошибок объекта вендора. Вызывается случае если сервер Яндекс ничего не ответил или ответил
     * системным сообщениемю При этом обработчику передается строка с ошибкой и исходный запрос,
     * а при наличии системного сообщения от сервера - выводим его в консоль.
     */
    errorHandler(message, cityName) {
        console.log(message);
        let result = `Не удалось получить данные по запросу: ${cityName}.`;
        this.callBack(result, false);
    }

    /**
     * @access private
     * @async
     * @borrows App~City
     * @param {JSON} geoData - геоданные
     * @return {City}
     * @desc
     * Создает экземпляры городов и наполняет их данными для последующего получения погоды
     */
    createCity(geoData) {
        let sourceCoordinates = geoData.InternalToponymInfo.Point.coordinates,
            sourceAddress = geoData.Address.Components,
            coordinates = {
                lat: sourceCoordinates[1],
                lon: sourceCoordinates[0]
            },
            address = [];
        for(let element of Object.values(sourceAddress)) {
            address.push(element.name);
        }
        return new City(address, coordinates)
    }
}