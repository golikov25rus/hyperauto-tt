/**
 * @author Denis Golikov <golikov25rus@gmail.com>
 */

import {conditions, windDirections, temperatureTypes} from '../enums/weather.js';
import Day from '../entities/day.js';

/**
 * @namespace App~Weather
 * @class Weather
 * @desc
 * Модель погоды
 */
export default class Weather {
    /**
     * @access public
     * @param {{fact: JSON, forecasts: JSON[]}} weather - JSON с данными о погоде
     * @param {City} city
     * @param {string} primaryTempType - основной тип температуры
     * @desc
     * Класс содержит в себе текущий день и массив дней с прогнозом погоды в каждом из этих дней а так же город/адрес
     * к которому данная модель относится
     */
    constructor(weather, city, primaryTempType) {
        let date = new Date(weather.fact['obs_time'] * 1000);
        /**
         * @access public
         * @type {string}
         */
        this.primaryTempType = primaryTempType;
        /**
         * @access public
         * @type {Day}
         */
        this.currentDay = new Day(date, this.parseWeather(weather.fact));
        /**
         * @access public
         * @type {Array}
         */
        this.daysWeather = this.createDays(weather.forecasts);
        this.city = city;
    }

    /**
     * @access private
     * @borrows App~Weather~Enums~Conditions
     * @borrows App~Weather~Enums~WindDirections
     * @borrows App~Weather~Enums~TemperatureTypes
     * @param {{fact: JSON, forecasts: JSON[]}} weather
     * @return {{condition: string, wind: {direction: string, speed: string}, temperature: {primary: string, alternative: string, celsius: {air: string, water: string}, fahrenheit: {air: string, water: string}}, iconPath: string, pressure: string, humidity: string}}
     * @desc
     * Разбираю исходный JSON
     */
    parseWeather(weather) {
        let temperature = 'temp' in weather ? weather['temp'] : weather['temp_avg'],
            celsiusInterval = this.getTemperatureInterval(temperature, weather['feels_like'], temperatureTypes.celsius),
            fahrenheitInterval = this.getTemperatureInterval(temperature, weather['feels_like'], temperatureTypes.fahrenheit),
            primaryTempType = this.primaryTempType,
            alternativeTempType = Object.entries(temperatureTypes).filter(([key,value]) => key !== primaryTempType)[0][0];
        return {
            condition: conditions[weather['condition']],
            wind: {
                direction: windDirections[weather['wind_dir']],
                speed: `${weather['wind_speed']} - ${weather['wind_gust']} м/с`,
            },
            temperature: {
                primary: primaryTempType,
                alternative: alternativeTempType,
                celsius: {
                    air: celsiusInterval,
                    water: weather['temp_water'] + temperatureTypes.celsius
                },
                fahrenheit: {
                    air: fahrenheitInterval,
                    water: this.celsiusToFahrenheit(weather['temp_water']) + temperatureTypes.fahrenheit
                }
            },
            iconPath: `https://yastatic.net/weather/i/icons/blueye/color/svg/${weather['icon']}.svg`,
            pressure: weather['pressure_mm'] + 'мм',
            humidity: weather['humidity'] + '%'
        };
    }

    /**
     * @access private
     * @param {JSON[]} forecasts - масив данных
     * @return {Array} - массив дней
     * @desc
     * Создает массив дней
     */
    createDays(forecasts) {
        let result = [];
        for(let forecast of forecasts) {
            let date = new Date(forecast.date);
            result.push(new Day(date, this.parseWeather(forecast.parts.day)))
        }
        return result;
    }

    /**
     * @access private
     * @param {number} currentTemperature
     * @param {number} feelsTemperature
     * @param {string} type - тип primary температуры
     * @return {string}
     * @desc
     * Полчить строковые интервалы из двум параметров температуры
     */
    getTemperatureInterval(currentTemperature, feelsTemperature, type) {
        if(type === temperatureTypes.fahrenheit) {
            currentTemperature = this.celsiusToFahrenheit(currentTemperature);
            feelsTemperature = this.celsiusToFahrenheit(feelsTemperature);
        }
        return  currentTemperature > feelsTemperature ? `${feelsTemperature}...${currentTemperature}${type}` : `${currentTemperature}...${feelsTemperature}${type}`;
    }

    /**
     * @access private
     * @param {number} temperature
     * @return {string}
     * @desc
     * Конвертация показаний температуры из Цельсия в Фарингейты с округлением до целого числа
     */
    celsiusToFahrenheit(temperature) {
        return ((temperature * 9/5) + 32).toFixed();
    }
}