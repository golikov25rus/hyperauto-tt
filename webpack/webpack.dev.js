const merge = require('webpack-merge'),
      common = require('./webpack.common.js'),
      path = require('path');

module.exports = merge(common, {
	mode: 'development',
	devtool: 'inline-source-map',
    watch: true,
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        ignored: /node_modules/
    },
    devServer: {
        index: 'index.html',
        host: 'localhost',
        port: 8080,
        allowedHosts: [
            'cloudapi.weather.yandex.net'
        ],
        contentBase: path.resolve(__dirname, '../'),
        publicPath: '/public',
        progress: true,
        inline: true,
        watchContentBase: true,
        lazy: false,
        open: true,
        hot: true,
        proxy: {
            '/api': {
                target: 'https://api.weather.yandex.ru/v1/forecast',
                pathRewrite: {'^/api' : ''},
                secure: false
            },
            changeOrigin: true
        }
    }
});
