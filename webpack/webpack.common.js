const path = require('path'),
      HtmlWebpackPlugin = require('html-webpack-plugin'),
	  MiniCssExtractPlugin = require("mini-css-extract-plugin"),
 	  CleanWebpackPlugin = require('clean-webpack-plugin'),
 	  HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');

module.exports = {
	entry: {
		common: ['./webpack/entries/common.js'],
    },
	plugins: [
		new CleanWebpackPlugin(['public'], { root: path.resolve(__dirname, '../')}),

		new HtmlWebpackPlugin({
            alwaysWriteToDisk: true,
            inject: false,
			template: path.resolve(__dirname, '../templates/index.html'),
			filename: path.resolve(__dirname, '../index.html'),
            chunks: ['common']
    	}),

		new MiniCssExtractPlugin({
			filename: '[hash].[name].weatherforecast.css',
			path: path.resolve(__dirname, '../public')
		}),

        new HtmlWebpackHarddiskPlugin(),
   ],
	output: {
		filename: '[hash].[name].weatherforecast.js',
		path: path.resolve(__dirname, '../public'),
		publicPath: '/public'
	},
	module: {
		rules: [
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: [{
					loader: 'file-loader',
                    options: {
                        name: 'images/[hash].[ext]'
                    }
                }],
			},
			{
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
	         	use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[hash].[ext]'
                    }
                }]
			},
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    'css-loader'
                ]
            }
		]
	}
};