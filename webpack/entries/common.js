// JS ASSETS FILES IMPORT START

import './../../src/js/index.js';
import './../../src/vendors/uikit/js/uikit-icons.min';
import './../../src/vendors/uikit/js/uikit.min';

// CSS ASSETS FILES IMPORT START

import './../../src/vendors/uikit/css/uikit-rtl.css';
import './../../src/vendors/uikit/css/uikit.css';
import '../../src/css/base.css';
import '../../src/css/index.css';
