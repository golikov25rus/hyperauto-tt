const path = require('path'),
      webpack = require('webpack'),
      webpackDevServer  = require('webpack-dev-server'),
      config = require('./webpack.dev.js');
config.entry.common.unshift(`webpack-dev-server/client?http://${config.devServer.host}:${config.devServer.port}/`);
const compiler = webpack(config),
      server = new webpackDevServer(compiler, config.devServer);


server.listen(config.devServer.port, config.devServer.host, () => {

});