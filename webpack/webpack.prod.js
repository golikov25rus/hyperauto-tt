const merge = require('webpack-merge'),
      common = require('./webpack.common.js'),
      UglifyJsPlugin = require('uglifyjs-webpack-plugin'),
      OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");


	module.exports = merge(common, {
		  mode: 'production',
		  optimization: {
				minimizer: [
					new UglifyJsPlugin({
						cache: true,
						parallel: true,
						sourceMap: true
					}),
					new OptimizeCSSAssetsPlugin({
						assetNameRegExp: /\.css$/g,
						cssProcessor: require('cssnano')
					})
				]
		  },
		module: {
			rules: [
				{
					test: /\.m?js$/,
					exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['env']
                        }
                    }
				}
			]
		}
	});
